import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import MainPage from './MainPage';
import PresentationForm from './PresentationForm'
import AttendConferenceForm from './AttendConferenceForm'

function App(props) {
  // Check if the 'attendees' prop is undefined, if so, return null
  // This is to handle cases where 'props.attendees' might be missing or not yet loaded
  if (props.attendees === undefined) {
    return null;
  }
  // If 'props.attendees' is defined, continue with rendering the component
  return (
    <BrowserRouter>
       {/* Render the 'Nav' component */}
      <Nav />
      {/* Create a container with class 'container' */}
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path='locations'>
            <Route path='new' element={<LocationForm />} />
          </Route>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path='attendees'>
            <Route index element={<AttendeesList attendees={props.attendees} />} />
            <Route path='new' element={<AttendConferenceForm />} />
          </Route>
          <Route path="presentations">
            <Route path="new" element={<PresentationForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}


export default App;
