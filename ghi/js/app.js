function createCard(name, location, description, start, end, pictureUrl) {
    return `
      <div class="card shadow-sm p-3 mb-5 bg-body-tertiary rounded gap-0 row-gap-3">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">${start} - ${end}</div>
      </div>
    `;
}
window.addEventListener('DOMContentLoaded', async () => {
    // Assigns the url that list all conferences to the url variable
    const url = 'http://localhost:8000/api/conferences/';

    try {
        // Fetches the url resource from the server
        // await - waits for the response to fulfill (needs to be an async function)
        const response = await fetch(url);
        // Triggers if the response.ok === false
        if (!response.ok) {
            // Figure out what to do when the response is bad
        } else {
            // Returns and assigns a promise that resolves to a JS object
            const data = await response.json();

            let col = 0;
            // Iterates over the data variables conferencese
            for (let conference of data.conferences) {
                // Assigns the URL for the conference to detailUrl
                const detailUrl = `http://localhost:8000${conference.href}`;
                // Fetches the url resource from the server
                // await - waits for the response to fulfill (needs to be an async function)
                const detailResponse = await fetch(detailUrl);
                // Triggers if the response.ok === true
                if (detailResponse.ok) {
                    // Returns and assigns a promise that resolves to a JS object
                    const details = await detailResponse.json();
                    // Lines 42-47 get specific data from the details object
                    const title = details.conference.name;
                    const location = details.conference.location.name;
                    const description = details.conference.description;
                    const start = new Date(details.conference.starts).toLocaleDateString();
                    const end = new Date(details.conference.ends).toLocaleDateString();
                    const pictureUrl = details.conference.location.picture_url;
                    // Passes the data from lines 42-47 into the createCard function to produce HTML
                    const html = createCard(title, location, description, start, end, pictureUrl);
                    // Selects the HTML id #col with number identified by variable 'col'
                    const column = document.querySelector(`#col-${col}`);
                    // Inserts the html from the variable 'html' into the 'column' variable
                    column.innerHTML += html;
                    col++;
                    // Resets the 'col' variable to 0 when the row fills all of the columns with conferences
                    if (col > 2) {
                        col = 0;
                    }
                }
            }
        }
    } catch(e) {
        // Figure out what to do if an error is raised
    }
});
