window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();

        // Get the select tag element by its id 'state'
        const locationTag = document.getElementById('location');
        // For each state in the states property of the data
        for (let location of data.locations) {
            // Create an 'option' element
            const locationOption = document.createElement('option');
            // Set the '.value' property of the option element to the
            // locations name
            locationOption.value = location.id;
            // Set the '.innerHTML' property of the option element to
            // the state's name
            locationOption.innerHTML = location.name;
            // Append the option element as a child of the select tag
            locationTag.appendChild(locationOption);
        };
    };
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "POST",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        console.log(response)
        console.log(json)
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference);
        }
    });
});
